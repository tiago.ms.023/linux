#! /bin/bash

echo "[******************************************]"
echo "Instalacao de uma instancia wordpress"
echo "[******************************************]"
echo "[******************************************]"
echo "QUAL É O USERNAME QUE PRETENDE PARA A INSTALAÇÃO DO WORDPRESS?"
read admin_username
echo "[******************************************]"
echo "[******************************************]"
echo "QUAL É A PASSWORD QUE PRETENDE PARA A INSTALAÇÃO DO WORDPRESS?"
read admin_password
echo "[******************************************]"

: '
echo "Leitura dos dados"
echo $admin_username
echo $admin_password
'
echo "[******************************************]"
echo "COMEÇAR A INSTALAR OS PACKAGES NECESSÁRIOS"
echo "[******************************************]"

# dnf module reset php (no caso da versao 7.2 ja estar instalada)
dnf module enable php:7.4 -y -v
dnf -v -y install mariadb-server httpd php php-mysqlnd dos2unix php-gd php-mbstring php-json
dnf -v install httpd php php-common php-mysqlnd php-mbstring php-gd mariadb-server
#mod_ssl -y
echo "[******************************************]"
echo "PACKAGES INSTALADOS"
echo "[******************************************]"

echo "[******************************************]"
echo "A INICIALIZAR O APACHE E MARIADB"
echo "[******************************************]"

#sed -i 's/AllowOverride none/AllowOverride all/g' /etc/httpd/conf/httpd.conf
systemctl start httpd && systemctl start mariadb
#systemctl status httpd
#system status mariadb


#PARA VERIFICAR SE A BASE DE DADOS JÁ EXISTE
DB_NAME=outcloud

RESULT=$(mysql --batch --skip-column-names -e "SHOW DATABASES LIKE '"$DB_NAME"';" | grep "$DB_NAME" > /dev/null; echo "$?")

#echo $RESULT

if [ $RESULT -eq 0 ]; then
	echo "[******************************************]"
	echo "DATABASE JÁ FOI CRIADA"
	echo "[******************************************]"
else 
	echo "[******************************************]"
	echo "A CRIAR BASE DE DADOS"
	echo "[******************************************]"

	echo "CREATE DATABASE $DB_NAME;" | mysql
	echo "GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$admin_username'@'localhost' IDENTIFIED BY '$admin_password';" | mysql

	

	echo "BASE DE DADOS CRIADA"
	echo "[******************************************]"
fi

echo "[******************************************]"
echo " DOWNLOAD DO WORDPRESS "
echo "[******************************************]"

curl -v -O https://wordpress.org/latest.tar.gz
tar zxvf latest.tar.gz && rm -f latest.tar.gz
mkdir /var/www/html/
mv -v wordpress/* /var/www/html
chown -R apache:apache /var/www/html

# Configure WordPress
cp -v /var/www/html/wp-config-sample.php /var/www/html/wp-config.php

sed -i "s@database_name_here@$DB_NAME@" /var/www/html/wp-config.php
sed -i "s@username_here@$admin_username@" /var/www/html/wp-config.php
sed -i "s@password_here@$admin_password@" /var/www/html/wp-config.php

: '
curl https://api.wordpress.org/secret-key/1.1/salt/ >> /var/www/html/wp-config.php

# Modify the .htaccess
cat << 'EOF' >> /var/www/html/.htaccess
# BEGIN WordPress

   RewriteEngine On
   RewriteBase /
   RewriteRule ^index\.php$ - [L]
   RewriteCond %{REQUEST_FILENAME} !-f
   RewriteCond %{REQUEST_FILENAME} !-d
   RewriteRule . /index.php [L]

# END WordPress"
EOF
chmod 666 /var/www/html/.htaccess
'

#cd /var/www/html/wordpress
#mkdir /var/www/html/wordpress/wp-content/{uploads,cache}
sed -i -v 's/AllowOverride none/AllowOverride all/g' /etc/httpd/conf/httpd.conf
systemctl enable --now httpd


dos2unix /var/www/html/wp-config.php
chown -R -v apache:apache /var/www/html

#permitir trafego nas portas 22,80 e 3306
firewall-cmd --permanent --add-port=22/tcp
firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --permanent --add-port=3306/tcp

#criar admin no wordpress

curl --data-urlencode "weblog_title= WORDPRESS_TEST" \
	--data-urlencode "user_name= $admin_username" \
	--data-urlencode "admin_password= $admin_password" \
	--data-urlencode "admin_password2= $admin_password" \
	--data-urlencode "admin_email= testeOutCloud@cloud.com" \
	--data-urlencode "Submit=Install+WordPress" \
	--silent --output /dev/null http://localhost/wp-admin/install.php?step=2
	 

#Para verificar o status do site
url='http://localhost/'
status=$(curl --head --location --connect-timeout 5 --write-out %{http_code} --silent --output /dev/null ${url})

echo "[******************************************]"
echo "RESULTADO DO PEDIDO REALIZADO NO LOCALHOST: "
echo "STATUS : $status "
echo "[******************************************]"

echo "FIM DA INSTALAÇÃO DO WORDPRESS - PARA TESTAR  -----> http://localhost/  "
echo "[******************************************]"

