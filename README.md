#  Exercicio para Linux

## 1)Instalar e configurar uma VM com CentOS 8

Foi necessário instalar a VirtualBox e ir ao site official do CentOS para descarregar a imagem Iso do CentOS 8. Depois disso, é necessário criar a máquina virtual onde crio dois discos.
Após ter a máquina virtual com dois discos é necessário ir aos settings/storage e adicionar a imagem iso do CentOS 8. Fazer start e instalar o SO no primeiro disco.

## Partições
### O que são e para que serve?

As partições servem para dividir o hard disk em secções isoladas, deste modo podemos organizar o disco em diversas áreas lógicas. Vantagens: Segurança, Possibilidade de usar mais que um filesystem, manegamento de disco eficiente.
Criar duas partições no disco 2 , uma com 8gb e outra com 2gb. Na partição de 8gb o filesystem deverá ser ext4 e na de 2gb deve ser realizado um swap.

### Ext4

O ext4 ou quarto sistema de arquivos estendido é um sistema de arquivos com diário amplamente usado para Linux.


### Swap o que é e para que serve?

O swap serve para o linux lidar com a falta de memória física em algumas situações. O swap space é o espaço no disco que pode ser usado como memória e é usado apenas em situação que a physical memory é usada além do limite. Usando a mesma memória fisica é possivel correr mais processos com swap space mas o swap space deve ser monitorizado de perto pois, se a sua utilização for muito elevada o desempenho do sistema diminuirá.

# Command line

## Criar partição de 8gb com filesystem ext4

parted -l (serve para identificar a hard drive que eu quero partir)  
parted /dev/sdb (começo a partição no segundo disco)  
mklabel msdos (dar uma label ao novo disco)  
mkpart (para criar a partição)  

Partition type:primary  
File system type: ext4  
Start: 1  
End:8000 (tamanho da partição 8gb)  
print (para ver os detalhes da nova partição)  
quit  

### Formatar a nova partição:  

mkfs.ext4 /dev/sdb1 (formatar a nova partição com o ext4 file)  
e4label /dev/sdb1 disk2-part1 (label da partição)  

#### Montar a nova partição ext4 no FileSystem:  

mkdir /mnt/disk2-part1 (criar a pasta disk2-part1 no diretorio mnt)  
mount /dev/sdb1 //mnt/disk2-part1 (mount da partição)  
df -hT (para visualizar todos os filesystem do meu Sistema e o seu respetivo tamanho)  

## Criar partição de 2gb com o swap:  

fdisk /dev/sdb (para iniciar a partição) 
   
p ( para verificar o espaço que tenho no disco)  
n (criação da partição)  
Partition type: p (primary)  
Partition number: default  
First sector: default  
Last sector: +2000 MB (partição de 2gb)  
  
p (verificar a criação da partição)  
l (print todos os tipos de partição disponiveis (82 é o número da partição swap))  
t (para mudar o tipo de partição)  
default (partição 2)  
82 (é o número identificar da partição swap )  
p (para verificar a partição swap)  
w (para confirmer a ação, sem este comando o fdisk não iria graver nada no disco)  
partprob /dev/sdb (para forçar o kernel a reler a partition table)  

## Criar um utilizador com o vosso nome e dar-lhe permissões sudo:  

Criei na instalação do Sistema operativo   

## Desactivar SELinux:  
### O que é SELinux?  

É uma camada extra de controlo de segurança. Em alguns casos não é necessário ter ativado e pode interferer com certos programas que se tente instalar.  


## Comandos:   
  
sestatus (para verificar o estado do SELinux)  
sudo setenforce 0 (para casos que não precisamos de fazer reboot do Sistema para realizar determinada tarefa)  
sudo nano /etc/selinux/config (abrir o ficheiro config do selinux (para mudar o selinux permanentemente)  
SELINUX=disabled (mudar a linha do selinux para disabled)  
sestatus (para verificar o estado do SELinux)    
    
# 2)Configurar uma instância wordpress  
  
sudo dnf module list php (verificar quais os modulos de php tem disponível para download)  
sudo dnf module reset PHP (para resetar os modulos e permitir que a versao 7.4 seja instalada)  
sudo dnf module enable php: 7.4 (habilitar o modulo 7.4 PHP)  
  
dnf install httpd php php-common php-mysqlnd php-mbstring php-gd mariadb-server  
php -v (para verificar que a nova versão é 7.4)  
sed -i ‘s/AllowOverride none/AllowOverride all/g’ /etc/httpd/conf/httpd.conf (editar a configuração do apache para perimitir o uso de hypertext acess)(não tenho a certeza se é preciso)  
  
systemctl start httpd && systemctl start mariadb (start dos serviços apache e mariadb )  
systemctl status httpd (para verificar se o apache está a correr)  
systemctl status mariadb (para verificar se o apache está a correr)  

## Tester no browser se o apache teste está a funcionar:  
   
ip a (ver qual é o ip)  
  
## Meter o ip no browser e verificar se aparece a página do apache teste   
  
### Criar a base de dados:  
  
mysql (entrar na base de dados mariadb)  
CREATE DATABASE wordpress;  
GRANT ALL PRIVILEGES ON wordpress.* TO ‘outcloudwp’@’localhost’ IDENTIFIED BY ‘outcloudsecret’;  
exit  
  
### Download do wordpress:   
  
curl -O https://wordpress.org/latest.tar.gz (download do wordpress)  
tar zxvf latest.tar.gz (realizar o extract: z significa gzip format, x é para extrair, f para que ficheiro (Julia slides))  
mkdir /var/www/html/ (criar a pasta html)  
mv wordpress/* /var/www/html/ (mover os ficheiros de instalação para a pasta html)  
chown -R apache:apache /var/www/html (para mudar as permissões no ficheiro html, desta maneira é possível fazer upload de ficheiros através do painel de controlo do wordpress)  

  
### Permitir trafego nas portas 22,80 e 3306  

sudo firewall-cmd --state (para verificar se a firewall está ativa)  
sudo firewall-cmd --permanent --add-port=22/tcp  
sudo firewall-cmd --permanent --add-port=80/tcp  
sudo firewall-cmd --permanent --add-port=3306/tcp  
sudo firewall-cmd --reload  
  
# IR ao http://IP-ADDRESS/wp-admin/setup-config.php para completar a instalação do wordpress  
   
### Observações:  
  
#### Dnf install? Qual a diferença para o yum install?   

Dnf é igual ao yum que instala,atualiza e remove packages do sistema linux baseado em RPM. O DNF é introduzido para melhorar alguns problemas do yum ( performance por exemplo)  


#### RPM? O que é RPM?  

O gerenciador de packages red Hat, comumente conhecido como RPM, é um sistema de gerenciamento de packages usado para gerenciar, instalar, desinstalar, atualizar, listar e verificar packages baseados em .rpm em distribuições Linux baseadas em Redhat.  
Os arquivos com a extensão .rpm no Linux são como a alternativa dos arquivos .exe no Windows. Os arquivos .rpm são usados para instalar aplicativos em qualquer sistema operacional Linux baseado em RedHat. É um formato de package  útil ao instalar software de terceiros em um sistema operacional.   


# 3) Criar um bash script que faça o pedido no ponto 2  
  
cd Área\ de\ Trabalho/  
touch wordpress.sh (criar um ficheiro)  
chmod +x worpress.sh (dar permissão ao script para ser executado)  
su (entrar em root)  
./wordpress.sh (para executar o script)  
  
## OBSERVAÇÕES:  
  
setsebool -P httpd_read_user_content 1 (caso de um erro 403 de Acess Denied)  
  
#### CASO O ROOT TENHA PASSWORD:  
  
Ir ao script na parte da criação da base de dados e colocar -p em todas chamadas mysql (são três) – se não fizer isto dá erro na criação da base de dados  
  
